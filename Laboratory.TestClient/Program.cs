﻿using System;
using System.Linq;

namespace Laboratory.TestClient
{
    class Program
    {
        /// <summary>
        /// Получение текста шаблона для получения всех измерений
        /// </summary>
        /// <param name="measures">Измерения строка с разделетелем запятая</param>
        /// <returns></returns>
        private static string GetTemplate(string measures)
        {
            return "{result.date} " + string.Join(" ", measures.Split(',').Select(x => x.Trim()).Select(x => $"{{{x}.in_channel}}: {{{x}.value}} {{{x}.units}}. {{{x}.comment}}"));
        }

        static void Main(string[] args)
        {
            // Получение объекта для логирования операций
            var logger = NLog.LogManager.GetCurrentClassLogger();

            // Создание сервиса для обращения к менеджеру Алтей
            var service = new DriverConnector.Connector();

            logger.Info("Connection created");

            var query = new Interfaces.Query { BeginDate = DateTime.Now.Date, EndDate = DateTime.Now.Date };

            // Получение списка доступных устройств
            var devices = service.GetDevices().ToList();

            // Считываем результаты по всем устройствам
            foreach (var device in devices)
            {
                // Получаем список всех результатов с устройства, с ключами измерений в поле данных
                var results = service.GetTemplateResults(device.Key, query, "{result.keys}").ToList();

                logger.Info($"{device.Name}: {device.Key} {device.Active}. Results: {results.Count}");

                // Получаем полный текст по результату
                foreach (var result in results)
                {
                    var id = result.Id;
                    var template = GetTemplate(result.Data);

                    try
                    {
                        var answer = service.GetTemplateResult(device.Key, id, template);
                        logger.Info($"{device.Key}:{id} {answer.Data}");
                    }
                    catch (Exception e)
                    {
                        logger.Error($"{device.Key}:{id} {template}");
                        logger.Error(e);
                        throw;
                    }
                }

                // Не удалять! Код для тестирования отметок о выполнении
                //var markResults = results.Take(results.Count / 2).Select(x => new Interfaces.MarkResult
                //{
                //    ResultId = x.Id,
                //    Used = true
                //}).ToList();

                //service.MarkResults(device.Key, markResults);

                var results2 = service.GetResults(device.Key, query);
                logger.Info($"{device.Name}: {device.Key} {device.Active}. Results2: {results2.Count}");
                foreach (var result in results2)
                {
                    var id = result.Id;

                    try
                    {
                        var answer = service.GetResult(device.Key, id);
                        logger.Info($"{device.Key}:{id} {answer.Data}");
                    }
                    catch (Exception e)
                    {
                        logger.Error($"{device.Key}:{id}");
                        logger.Error(e);
                        throw;
                    }
                }
            }
        }
    }
}
