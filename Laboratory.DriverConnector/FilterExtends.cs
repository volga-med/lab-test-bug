﻿namespace Laboratory.DriverConnector
{
    public static class FilterExtends
    {
        public static DMSrv.TSampleFilter AsSampleFilter(this Interfaces.Query query, string deviceKey)
        {
            var filter = new DMSrv.TSampleFilter();
            if (query.BeginDate.HasValue)
            {
                filter.sfBeginDate = query.BeginDate.Value;
            }

            if (query.EndDate.HasValue)
            {
                filter.sfEndDate = query.EndDate.Value;
            }

            filter.sfDeviceId = deviceKey;

            return filter;
        }
    }
}
