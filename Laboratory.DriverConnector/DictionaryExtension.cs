﻿using System;
using System.Collections.Generic;

namespace Laboratory.DriverConnector
{
    public static class DictionaryExtension
    {
        public static void AddIfRequested(this Dictionary<string, string> dict, string key, Func<string> getter, HashSet<string> requestedKeys)
        {
            if (requestedKeys.Contains(key))
            {
                dict.Add(key, getter());
            }
        }
    }
}
