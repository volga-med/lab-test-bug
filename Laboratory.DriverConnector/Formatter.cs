﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace Laboratory.DriverConnector
{
    internal class Formatter
    {
        private string GetXml(DMSrv.ISample sample)
        {
            if (sample.DeviceSample != null)
            {
                XmlDocument document = new XmlDocument();
                XmlElement element = document.CreateElement("result");
                string birthdate = string.Format("{0}", sample.DeviceSample.PatientBirthday);
                element
                    .AppendWith("result_id", sample.ID)
                    .AppendWith("result_date", sample.SampleDate)
                    .AppendWith("result_type", sample.DeviceSample.SampleType)
                    .AppendWith("result_key", sample.Key)
                    .AppendWith("result_barcode", sample.Barcode)
                    .AppendWith("result_priority", sample.DeviceSample.SamplePrioritetType)
                    .AppendWith("result_code", sample.DeviceSample.SampleCode)
                    .AppendWith("patient_lastname", sample.DeviceSample.PatientSurname)
                    .AppendWith("patient_firstname", sample.DeviceSample.PatientName)
                    .AppendWith("patient_patronym", sample.DeviceSample.PatientPatronym)
                    .AppendWith("patient_birtdate", birthdate)
                    .AppendWith("patient_sex", sample.DeviceSample.PatientSexType)
                    .AppendWith("patient_code", sample.DeviceSample.PatientCode);

                if (sample.DeviceSample.ResultList != null && sample.DeviceSample.ResultList.Count > 0)
                {
                    AppendingWithResults(element, sample.DeviceSample.ResultList);
                }

                using (var stringWriter = new StringWriter())
                {
                    using (var xmlTextWriter = new XmlTextWriter(stringWriter))
                    {
                        document.AppendChild(element);
                        document.WriteTo(xmlTextWriter);
                        return stringWriter.ToString();
                    }
                }

            }
            return string.Empty;
        }

        private void AppendingWithResults(XmlElement element, DMSrv.IResultList resultList)
        {
            for (int i = resultList.Count - 1; i >= 0; i--)
            {
                var measureResult = resultList.MeasureResult[i];

                for (int j = measureResult.MethodsResultList.MethodsResultCount - 1; j >= 0; j--)
                {
                    var methodResult = measureResult.MethodsResultList.MethodsResult[j];
                    string isDefaultMethod = methodResult.Methods.IsDefaultMethod ? "1" : "0";
                    XmlElement method = element.OwnerDocument.CreateElement("method")
                        .AppendWith("measure_id", measureResult.ID)
                        .AppendWith("method_id", methodResult.MethodsResultId)
                        .AppendWith("method_is_default", isDefaultMethod)
                        .AppendWith("method_name", methodResult.Methods.MethodName)
                        .AppendWith("method_key", methodResult.Methods.ExternalKey)
                        .AppendWith("method_date", methodResult.MethodsResult.ResultDate)
                        .AppendWith("method_in_channel", methodResult.MethodsResult.InChannel)
                        .AppendWith("method_comment", methodResult.MethodsResult.Comment)
                        .AppendWith("method_units", methodResult.MethodsResult.Units)
                        .AppendWith("method_biomaterial_code", methodResult.MethodsResult.BioMaterialCode)
                        .AppendWith("method_norm", methodResult.MethodsResult.Norm)
                        .AppendWith("method_type", methodResult.MethodsResult.ResultType)
                        .AppendWith("method_value", methodResult.MethodsResult.AsString);

                    var ragneResult = methodResult.MethodsResult as DMSrv.IRangeResult;
                    if (ragneResult != null)
                    {
                        if (!ragneResult.IsEmptyMin)
                        {
                            method.AppendWith("method_min_value", ragneResult.MinValue);
                        }
                        if (!ragneResult.IsEmptyMax)
                        {
                            method.AppendWith("method_max_value", ragneResult.MaxValue);
                        }
                    }
                    element.AppendChild(method);
                }
            }
        }

        private Dictionary<string, string> GetValues(DMSrv.ISample sample, HashSet<string> requestedKeys)
        {
            if (sample.DeviceSample != null)
            {
                var resultList = sample.DeviceSample.ResultList;
                var values = new Dictionary<string, string>(14 + requestedKeys.Count * resultList.Count);
                var keys = new List<string>(resultList.Count);

                values.AddIfRequested("result.id", () => sample.ID, requestedKeys);
                values.AddIfRequested("result.date", () => sample.SampleDate.ToString(), requestedKeys);
                values.AddIfRequested("result.type", () => sample.DeviceSample.SampleType.ToString(), requestedKeys);
                values.AddIfRequested("result.key", () => sample.Key, requestedKeys);
                values.AddIfRequested("result.barcode", () => sample.Barcode, requestedKeys);
                values.AddIfRequested("result.priority", () => sample.DeviceSample.SamplePrioritetType.ToString(), requestedKeys);
                values.AddIfRequested("result.code", () => sample.DeviceSample.SampleCode, requestedKeys);
                values.AddIfRequested("patient.name", () => sample.DeviceSample.PatientSurname + " " + sample.DeviceSample.PatientName, requestedKeys);
                values.AddIfRequested("patient.lastname", () => sample.DeviceSample.PatientSurname, requestedKeys);
                values.AddIfRequested("patient.firstname", () => sample.DeviceSample.PatientName, requestedKeys);
                values.AddIfRequested("patient.patronym", () => sample.DeviceSample.PatientPatronym, requestedKeys);
                values.AddIfRequested("patient.birtdate", () => string.Format("{0}", sample.DeviceSample.PatientBirthday), requestedKeys);
                values.AddIfRequested("patient.sex", () => sample.DeviceSample.PatientSexType.ToString(), requestedKeys);
                values.AddIfRequested("patient.code", () => sample.DeviceSample.PatientCode, requestedKeys);

                for (int i = resultList.Count - 1; i >= 0; i--)
                {
                    var measureResult = resultList.MeasureResult[i];
                    for (int j = measureResult.MethodsResultList.MethodsResultCount - 1; j >= 0; j--)
                    {
                        var methodResult = measureResult.MethodsResultList.MethodsResult[j];
                        var key = methodResult.Methods.ExternalKey + ".";
                        if (requestedKeys.Contains("result.keys"))
                        {
                            keys.Add(methodResult.Methods.ExternalKey);
                        }

                        values.AddIfRequested(key + "id", () => methodResult.MethodsResultId, requestedKeys);
                        values.AddIfRequested(key + "name", () => methodResult.Methods.MethodName, requestedKeys);
                        values.AddIfRequested(key + "date", () => methodResult.MethodsResult.ResultDate.ToString(), requestedKeys);
                        values.AddIfRequested(key + "in_channel", () => methodResult.MethodsResult.InChannel, requestedKeys);
                        values.AddIfRequested(key + "comment", () => methodResult.MethodsResult.Comment, requestedKeys);
                        values.AddIfRequested(key + "units", () => methodResult.MethodsResult.Units, requestedKeys);
                        values.AddIfRequested(key + "biomaterial_code", () => methodResult.MethodsResult.BioMaterialCode, requestedKeys);
                        values.AddIfRequested(key + "norm", () => methodResult.MethodsResult.Norm, requestedKeys);
                        values.AddIfRequested(key + "type", () => methodResult.MethodsResult.ResultType.ToString(), requestedKeys);
                        values.AddIfRequested(key + "value", () => methodResult.MethodsResult.AsString, requestedKeys);

                        if (requestedKeys.Contains(key + "min_value") || requestedKeys.Contains(key + "max_value"))
                        {
                            var ragneResult = methodResult.MethodsResult as DMSrv.IRangeResult;
                            if (ragneResult != null)
                            {
                                if (!ragneResult.IsEmptyMin)
                                {
                                    values.AddIfRequested(key + "min_value", () => ragneResult.MinValue.ToString(), requestedKeys);
                                }
                                if (!ragneResult.IsEmptyMax)
                                {
                                    values.AddIfRequested(key + "max_value", () => ragneResult.MaxValue.ToString(), requestedKeys);
                                }
                            }
                        }
                    }
                }

                values.AddIfRequested("result.keys", () => string.Join(",", keys), requestedKeys);
                return values;
            }
            else
            {
                return null;
            }
        }

        private string GetTemplate(DMSrv.ISample sample, string template)
        {
            var requestKeys = new HashSet<string>(Regex.Split(template, "\\{(.*?)\\}").Where(x => !string.IsNullOrEmpty(x)));

            var data = new StringBuilder(template, template.Length * 2);
            var values = GetValues(sample, requestKeys);
            foreach (string key in values.Keys)
            {
                data.Replace("{" + key + "}", values[key]);
            }
            return data.ToString();
        }

        public Interfaces.Result GetTemplateResult(DMSrv.ISample sample, string template)
        {
            return new Interfaces.Result
            {
                Id = sample.ID,
                Date = sample.SampleDate,
                Data = GetTemplate(sample, template)
            };
        }

        public Interfaces.Result GetResult(DMSrv.ISample sample)
        {
            return new Interfaces.Result
            {
                Id = sample.ID,
                Date = sample.SampleDate,
                Data = GetXml(sample)
            };
        }
    }
}
