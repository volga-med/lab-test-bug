﻿using Laboratory.DriverConnector.Exceptions;
using System;
using System.Collections.Generic;

namespace Laboratory.DriverConnector
{
    public class Connector
    {
        private readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly Formatter formatter;

        public Connector()
        {
            this.formatter = new Formatter();
        }

        #region ALTEY COM

        private DMSrvConnector.DMSrvConnection _dmconnection;

        /// <summary>
        /// Получение переиспользуемого сервиса Алтей
        /// </summary>
        /// <returns></returns>
        private DMSrvConnector.DMSrvConnection GetDMSrvConnection()
        {
            if (_dmconnection == null)
            {
                _dmconnection = new DMSrvConnector.DMSrvConnection();
            }

            if (!_dmconnection.Active)
            {
                if (!_dmconnection.Start())
                {
                    var err = _dmconnection.ConnectError;
                    _dmconnection = null;
                    throw new DMSrvConnectionStartException(err);
                }
            }

            return _dmconnection;
        }

        private DMSrv.IRemoteControlKernel GetRemoteControlKernel()
        {
            var service = GetDMSrvConnection();

            var controlKernel = service.DMSrv as DMSrv.IRemoteControlKernel;
            if (controlKernel != null)
            {
                return controlKernel;
            }
            else
            {
                throw new DMSrvNotAvailableException();
            }
        }

        /// <summary>
        /// Получение списка устройств
        /// </summary>
        /// <returns></returns>
        public List<Interfaces.Device> GetDevices()
        {
            try
            {
                var controlKernel = GetRemoteControlKernel();

                var deviceList = controlKernel.DeviceList;
                List<Interfaces.Device> devices = new List<Interfaces.Device>(deviceList.DeviceCount);
                for (int i = deviceList.DeviceCount - 1; i >= 0; i--)
                {
                    var device = deviceList.DeviceByIndex[i];
                    if (device != null)
                    {
                        devices.Add(
                           new Interfaces.Device
                           {
                               Key = device.ExternalKey,
                               Name = device.DeviceName,
                               Active = device.Active,
                               Endpoint = string.Empty
                           }
                        );
                    }
                }
                return devices;

            }
            catch (Exception e)
            {
                logger.Error(e);
                throw;
            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        private DMSrv.SampleList GetSampleList(string deviceKey, Interfaces.Query query)
        {
            var controlKernel = GetRemoteControlKernel();
            var sampleProxy = controlKernel.SampleServiceProxy;
            DMSrv.SampleList sampleList;
            string error;
            if (sampleProxy.LoadDeviceSampleList(deviceKey, query.BeginDate, query.EndDate, out sampleList, out error))
            {
                if (sampleList != null)
                {
                    return sampleList;
                }
                else
                {
                    throw new ArgumentNullException(nameof(sampleList));
                }
            }
            else
            {
                throw new LoadDeviceSampleListException(error);
            }
        }

        private DMSrv.ISample GetSample(string deviceKey, string sampleId)
        {
            var controlKernel = GetRemoteControlKernel();
            var sampleProxy = controlKernel.SampleServiceProxy;
            DMSrv.ISample sample;
            string error;
            if (sampleProxy.LoadSample(sampleId, out sample, out error))
            {
                if (sample != null)
                {
                    return sample;
                }
                else
                {
                    throw new ArgumentNullException(nameof(sample));
                }
            }
            else
            {
                throw new LoadSampleException(error);
            }
        }

        #endregion

        /// <summary>
        /// Получение списка результатов с формированием описания по шаблону
        /// </summary>
        /// <param name="deviceKey"></param>
        /// <param name="query"></param>
        /// <param name="template"></param>
        /// <returns></returns>
        public List<Interfaces.Result> GetTemplateResults(string deviceKey, Interfaces.Query query, string template)
        {
            try
            {
                var sampleList = GetSampleList(deviceKey, query);

                var items = new List<Interfaces.Result>(sampleList.Count);
                for (int i = sampleList.Count - 1; i >= 0; i--)
                {
                    var sample = sampleList.Sample[i];
                    if (sample != null)
                    {
                        items.Add(formatter.GetTemplateResult(sample, template));
                    }
                }
                return items;
            }
            catch (Exception e)
            {
                logger.Error(e);
                throw;
            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        public List<Interfaces.Result> GetResults(string deviceKey, Interfaces.Query query)
        {
            try
            {
                var sampleList = GetSampleList(deviceKey, query);

                var items = new List<Interfaces.Result>(sampleList.Count);
                for (int i = sampleList.Count - 1; i >= 0; i--)
                {
                    var sample = sampleList.Sample[i];
                    if (sample != null)
                    {
                        items.Add(formatter.GetResult(sample));
                    }
                }
                return items;
            }
            catch (Exception e)
            {
                logger.Error(e);
                throw;
            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        /// <summary>
        /// Получение результата с формированием текста по шаблону
        /// </summary>
        /// <param name="deviceKey"></param>
        /// <param name="sampleId"></param>
        /// <param name="template"></param>
        /// <returns></returns>
        public Interfaces.Result GetTemplateResult(string deviceKey, string sampleId, string template)
        {
            try
            {
                var sample = GetSample(deviceKey, sampleId);
                var result = formatter.GetTemplateResult(sample, template);
                return result;
            }
            catch (Exception e)
            {
                logger.Error(e);
                throw;
            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        public Interfaces.Result GetResult(string deviceKey, string resultId)
        {
            try
            {
                var sample = GetSample(deviceKey, resultId);
                var result = formatter.GetResult(sample);
                return result;
            }
            catch (Exception e)
            {
                logger.Error(e);
                throw;
            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        public bool MarkResults(string deviceKey, IEnumerable<Interfaces.MarkResult> markResults)
        {
            try
            {
                var controlKernel = GetRemoteControlKernel();
                var sampleProxy = controlKernel.SampleServiceProxy;
                string error;

                foreach (var markResult in markResults)
                {
                    DMSrv.ISample sample;
                    if (sampleProxy.LoadSample(markResult.ResultId, out sample, out error))
                    {
                        if (sample != null)
                        {
                            var resList = sample.DeviceSample.ResultList;
                            for (int i = resList.Count - 1; i >= 0; i--)
                            {
                                sampleProxy.TakeResult(resList.MeasureResult[i].ID, markResult.Used);
                            }
                        }
                    }
                    else
                    {
                        logger.Warn($"LoadSample by id {markResult.ResultId} error: {error}");
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Error(e);
                throw;
            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }
    }
}
