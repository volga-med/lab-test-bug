﻿using System.Xml;

namespace Laboratory.DriverConnector
{
    public static class XmlFormatterExtension
    {
        public static XmlElement AppendWith(this XmlElement element, string name, object value)
        {
            var attribute = element.OwnerDocument.CreateAttribute(name);
            attribute.Value = value != null ? value.ToString() : string.Empty;
            element.Attributes.Append(attribute);
            return element;
        }
    }
}
