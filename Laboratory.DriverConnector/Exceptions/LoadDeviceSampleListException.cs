﻿using System;

namespace Laboratory.DriverConnector.Exceptions
{
    public class LoadDeviceSampleListException : Exception
    {
        public LoadDeviceSampleListException(string message) : base(message)
        {
        }
    }
}
