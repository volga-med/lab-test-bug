﻿using System;

namespace Laboratory.DriverConnector.Exceptions
{
    public class DMSrvNotAvailableException : Exception
    {
        public DMSrvNotAvailableException() : base("Can't take DMSrv.IRemoteControlKernel")
        {
        }
    }
}
