﻿using System;

namespace Laboratory.DriverConnector.Exceptions
{
    public class LoadSampleException : Exception
    {
        public LoadSampleException(string message) : base(message)
        {
        }
    }
}
