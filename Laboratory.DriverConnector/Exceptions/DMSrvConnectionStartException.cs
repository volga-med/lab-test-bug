﻿using System;

namespace Laboratory.DriverConnector.Exceptions
{
    public class DMSrvConnectionStartException : Exception
    {
        public DMSrvConnectionStartException(string message) : base(message)
        {
        }
    }
}
