﻿using System;
using System.Runtime.Serialization;

namespace Laboratory.Interfaces
{
    [DataContract]
    public class Result
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public DateTime? Date { get; set; }

        [DataMember]
        public string Data { get; set; }
    }
}
