﻿using System;
using System.Runtime.Serialization;

namespace Laboratory.Interfaces
{
    [DataContract]
    public class Query
    {
        [DataMember]
        public DateTime? BeginDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }
    }
}
