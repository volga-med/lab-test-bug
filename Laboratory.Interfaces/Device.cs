﻿using System.Runtime.Serialization;

namespace Laboratory.Interfaces
{
    [DataContract]
    public class Device
    {
        [DataMember]
        public string Key { get; set; }

        [DataMember]
        public string Name { get; set; }
                
        [DataMember]
        public string Endpoint { get; set; }
        
        [DataMember]
        public bool Active { get; set; }
    }
}
