﻿using System.Runtime.Serialization;

namespace Laboratory.Interfaces
{
    [DataContract]
    public class MarkResult
    {
        [DataMember]
        public string ResultId { get; set; }

        [DataMember]
        public bool Used { get; set; }
    }
}
